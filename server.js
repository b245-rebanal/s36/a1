const express = require("express");
const mongoose = require("mongoose");

const taskRoute =require('./Routes/taskRoute.js');

const app = express();
const port = 3001


	//MongoDB connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-rebanal.hxprcml.mongodb.net/?retryWrites=true&w=majority", 
			{
				//Allows us to avoid any current and future errors while connecting to MONGODB
				useNewUrlParser: true,
				useUnifiedTopology: true

		})

	//Check connection

	let db = mongoose.connection;

	//error catcher
	db.on("error", console.error.bind(console, "Connection Error!"))

	//Confirmation of the connection
	db.once("open", ()=>console.log("We are now connected to the cloud!"))


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// routing
//localhost:3001/tasks/get
app.use("/tasks", taskRoute);







app.listen(port, () => console.log(`Server is running at port ${port}!`))

/*
	Separation of concerns:
		1. Model should be connected to the controller.
		2. Controller should be connected to the Routes.
		3. Route should be connected to the server/application.
		
*/

